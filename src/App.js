import React, {useEffect} from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";
import './assets/App.css';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
    faPlus,
    faMinus,
    fas,
    faSync,
    faClock,
    faEye,
    faBuilding,
    faCity,
    faBriefcase
} from '@fortawesome/free-solid-svg-icons';
import Task1 from "./containers/task1/Task1";
import Task2 from "./containers/task2/Task2";
import Task3 from "./containers/task3/Task3";
import Task4 from "./containers/task4/Task4";
import Task5 from "./containers/task5/Task5";
import Cabinet from "./containers/cabinet/Cabinet";
import HeaderAll from "./components/headerall/HeaderAll";
import Vacancy from "./components/vacancy/Vacancy";



library.add(fas, faPlus, faMinus, faSync, faClock, faEye, faBuilding, faCity, faBriefcase )

function App() {
  return (
    <div className="App">
        <Router>
            <HeaderAll/>
            <Switch>
                <Route exact path="/filtered">
                    <Task5/>
                </Route>
                <Route exact path="/vacancy">
                    <Task4/>
                </Route>
                <Route path="/vacancy/:id" component={Vacancy} />
                <Route path="/task3">
                    <Task3/>
                </Route>
                <Route path="/cabinet" render={props => localStorage.getItem('user') ? <Cabinet/> : <Redirect to ='/task3' />}/>
                <Route path="/task2">
                    <Task2/>
                </Route>
                <Route path="/task1">
                    <Task1/>
                </Route>
            </Switch>
        </Router>
    </div>
  );
}
// const mapStateToProps = state =>{
//     return {
//         vacanciesData : state.vacancies
//
//     }
// }
//
// const mapDispatchToProps = dispatch => {
//     return {
//         fetchVacancies: () =>{dispatch(fetchVacancies())}
//     }
// }



export default App;
