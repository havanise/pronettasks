import {combineReducers} from "redux";
import vacanciesReducer from "./reducers/vacanciesReducer"
import testReducer from "./reducers/testReducer"

const rootReducer = combineReducers({
    vacancies: vacanciesReducer,
    test : testReducer
})

export default rootReducer;