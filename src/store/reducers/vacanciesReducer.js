const initialState = {
    loadingVacancies: false,
    vacanciesData: [],
    filteredVacancies: [],
    category : [],
    subcategory: [],
    error: ""
}

const vacanciesReducer = (state = initialState, action) => {
    switch (action.type) {
        case "FETCH_REQUEST":
            return {
                ...state,
                loadingVacancies: true,
            }
        case "FETCH_SUCCESS":
            return {
                ...state,
                loadingVacancies: false,
                vacanciesData: action.payload,
            }
        case "FETCH_FAILURE":
            return {
                ...state,
                loadingVacancies: false,
                error: action.payload,
            }
        case "FETCH_FILTERED_SUCCESS":
            return {
                ...state,
                loadingVacancies: false,
                filteredVacancies: action.payload,
            }
        case "FETCH_FILTERED_VACANCIES":
            return {
                ...state,
                loadingVacancies: false,
                filteredVacancies: action.payload,
            }
        case "FETCH_SELECT_SUCCESS":
            return {
                ...state,
                category: action.payload,
            }
        case "FETCH_SUBSELECT_SUCCESS":
            return {
                ...state,
                subcategory: action.payload,
            }
        default: return state
    }
}
export default vacanciesReducer;
