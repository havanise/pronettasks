const initialState = {
    loadingVacancy: false,
    vacancyData: {},
    error: ""
}

const testReducer = (state = initialState, action) => {
    switch (action.type) {
        case "FETCH_REQUEST_DETAIL":
            return {
                ...state,
                loadingVacancy: true,

            }
        case "FETCH_SUCCESS_DETAIL":
            return {
                ...state,
                loadingVacancy: false,
                vacancyData: action.payload,
            }
        case "FETCH_FAILURE_DETAIL":
            return {
                ...state,
                loadingVacancy: false,
                error: action.payload,
            }
        default: return state

    }
}
export default testReducer;
