import axios from "axios"
export const fetchVacancy = (params) =>{
    return (dispatch) =>{
        dispatch(fetchRequestDetail())
        axios.get("https://devjobscore.prospectsmb.com/v1/vacancies/" + params.id)
            .then(response=>{
                const vacancyData = response.data.data;
                dispatch(fetchSuccessDetail(vacancyData))
            })
            .catch(error=>{
                const errorMsg = error.response.data.error.message;
                dispatch(fetchFailureDetail(errorMsg))
            })
    }
}



export const fetchRequestDetail = () =>{
    return {
        type: 'FETCH_REQUEST_DETAIL'
    }
}
export const fetchSuccessDetail = (vacancyData) =>{
    return {
        type: "FETCH_SUCCESS_DETAIL",
        payload: vacancyData,
    }
}
export const fetchFailureDetail = (error) =>{
    return {
        type: "FETCH_FAILURE_DETAIL",
        payload: error,
    }
}