import axios from "axios"

export const UrlFunc =(filters)=>{
    let defaultUrl = "https://devjobscore.prospectsmb.com/v1/vacancies";
    let url = "";
    filters.category && filters.position? url= defaultUrl +"?category=" +filters.category + "&position="+ filters.position:
        filters.category? url = defaultUrl + "/?category="+ filters.category:
            url= defaultUrl;
    return url
};
export const fetchSelectName = () =>{
    return (dispatch) =>{
        axios.get("https://devjobscore.prospectsmb.com/v1/categories/statistics")
            .then(response=>{
                const category=response.data.data;
                    dispatch(fetchSelect(category))
                })
            .catch(error=>{
                const errorMsg = error.message;
                dispatch(fetchFailure(errorMsg))
            })
    }
}
export const fetchSelectSubFunc = (value) =>{
    return (dispatch) =>{
        axios.get("https://devjobscore.prospectsmb.com/v1/category/"+ value)
            .then(response=>{
                const subcategory=response.data.data;
                dispatch(fetchSelectSub(subcategory))
            })
            .catch(error=>{
                const errorMsg = error.message;
                dispatch(fetchFailure(errorMsg))
            })
    }
}
export const fetchVacancies = () =>{
    return (dispatch) =>{
        dispatch(fetchRequest())
        axios.get("https://devjobscore.prospectsmb.com/v1/vacancies")
            .then(response=>{
                const vacanciesData = response.data.data;
                dispatch(fetchSuccess(vacanciesData))
                dispatch(fetchFilteredSuccess(vacanciesData))
            })
            .catch(error=>{
                const errorMsg = error.response.data.error.message;
                dispatch(fetchFailure(errorMsg))
            })
    }
}

export const fetchFilteredVacanciesFunc = (filters) =>{
    return (dispatch) =>{
        dispatch(fetchRequest())
        axios.get(UrlFunc(filters))
            .then(response=>{
                const filteredVacancies = response.data.data;
                dispatch(fetchFilteredVacancies(filteredVacancies))
            })
            .catch(error=>{
                const errorMsg = error.response.data.error.message;
                dispatch(fetchFailure(errorMsg))
            })
    }
}

export const fetchRequest = () =>{
    return {
        type: 'FETCH_REQUEST'
    }
}
export const fetchSuccess = (vacanciesData) =>{
    return {
        type: "FETCH_SUCCESS",
        payload: vacanciesData,
    }
}
export const fetchFilteredSuccess = (filteredVacancies) =>{
    return {
        type: "FETCH_FILTERED_SUCCESS",
        payload: filteredVacancies,
    }
}
export const fetchFilteredVacancies = (filteredVacancies) =>{
    return {
        type: "FETCH_FILTERED_VACANCIES",
        payload: filteredVacancies,
    }
}
export const fetchFailure = (error) =>{
    return {
        type: "FETCH_FAILURE",
        payload: error,
    }
}
export const fetchSelect = (category) =>{
    return {
        type: "FETCH_SELECT_SUCCESS",
        payload: category,
    }
}
export const fetchSelectSub = (subcategory)=>{
    return {
        type: "FETCH_SUBSELECT_SUCCESS",
        payload: subcategory,
    }
}