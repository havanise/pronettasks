import React, {useEffect} from 'react';
import {useParams} from "react-router-dom";
import "./vacancy.css"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {fetchVacancy} from "../../store/actions/vacancyActions";
import Loader from "react-loader-spinner";
import VacancyHeader from "../vacancyheader/VacancyHeader";
import Content from "../content/Content";
import Contact from "../contact/Contact";
import {connect} from "react-redux";

const Vacancy = ({vacancyData, fetchVacancy}) => {
    let params = useParams()
    useEffect(() => {
       fetchVacancy(params)
    }, [])

    if (vacancyData.loadingVacancy){
        return <div className={"spinner"}>
            <Loader type="ThreeDots" color="#1b8ef9"/>
        </div>
    }
    else if (vacancyData.error){
        return <div>
            {vacancyData.error}
        </div>
    }
    else if (vacancyData.vacancyData){
        return (
            <div className="vacancy">
                <div className={"about_vacancy"}>
                    <VacancyHeader data = {vacancyData.vacancyData}/>
                    <Content data = {vacancyData.vacancyData}/>
                    <Contact data={vacancyData.vacancyData} />
                </div>
                <div className={"views"}>
                    <p><FontAwesomeIcon icon="eye" className={"icon"}/>{vacancyData.vacancyData.viewsCount}</p>
                    <p><FontAwesomeIcon icon="clock"
                                        className={"icon"}/>{vacancyData.vacancyData.createdAt ? vacancyData.vacancyData.createdAt.substring(0, 10) : "qeyd olunmayıb"}
                    </p>
                </div>
            </div>
        )}
}

const mapStateToProps = state =>{
    return {
        vacancyData : state.test
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchVacancy: params => dispatch(fetchVacancy(params))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Vacancy);
