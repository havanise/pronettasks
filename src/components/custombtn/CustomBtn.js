import React from 'react';
import "./customBtn.css"
import PropTypes from 'prop-types';

const CustomBtn = (props) => {

    return (
        <button className={props.class} onClick={props.onClick} type={"submit"}>
            {props.text}
        </button>
    );


}
CustomBtn.propTypes = {
    class: PropTypes.string,
    text: PropTypes.string
};
export default CustomBtn;
