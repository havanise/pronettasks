import React, {useState} from 'react';
import "./clicker.css";
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


const Clicker = (props) => {
    const {defaultValue = 0} = props;
    const [values, setValues] = useState(defaultValue);

    let resetHandler=()=>{
        setValues(0)
    }
    let decrementHandler = ()=>{
        setValues(values - 1)
    }
    let incrementHandler = () =>{
        setValues(values + 1)
    }
    return (
        <div className="clicker">
            <div className="display">
                <div className="number">{values}</div>
            </div>
            <div className="buttons">
                <button className="btn white" style={{backgroundColor: "#28A745"}} onClick={incrementHandler} >
                    <FontAwesomeIcon icon="plus" />
                </button>
                <button className="btn" style={{backgroundColor: "#FFC107"}} onClick={resetHandler}>
                    <FontAwesomeIcon icon="sync" />
                </button>
                <button className="btn white disbtn" style={{backgroundColor: "red"}} onClick={decrementHandler} disabled={values === 0}>
                    <FontAwesomeIcon icon="minus" />
                </button>
            </div>

        </div>
    );
}
Clicker.propTypes = {
    defaultValue: PropTypes.number
};
export default Clicker;
