import React, {useState, useCallback} from 'react';
import "./registration.css"

import CustomBtn from "../custombtn/CustomBtn";
import Input from "../inputfield/Input";
import {useHistory} from "react-router-dom";



const Registration = (props) => {
    const [inputData, setInputData] = useState({
        firstName: '',
        lastName: '',
        email: '',
        password: ''
    })
    const [errorState, setErrorState] = useState({})
    const {firstName, lastName, email, password} = inputData
    let history = useHistory();


    const inputOnchangeHandler = (event, identifier) => {
        let updatedInputData = { ...inputData };
        updatedInputData[identifier] = event.target.value;
        setInputData(updatedInputData);
    }
    const handleSendAction = useCallback((e)=>{

        const errors ={};
        e.preventDefault();
        if (firstName === '' || firstName === null){
            errors.firstName = 'First name is required';
        }
        if (lastName === '' || lastName === null){
            errors.lastName = 'Last name is required';
        }
        if (email === '' || email === null){
            errors.email = 'Email is required';
        }
        if (password === '' || password === null){
            errors.password = 'Password is required';
        }
        if (Object.keys(errors).length === 0) {
            setErrorState({})
            localStorage.setItem('user', firstName)
           setInputData({
               email: '',
               lastName:  '',
               firstName: '',
               password:  ''
           })
            props.hide(!props.show)
            window.alert("Registration completed")
            history.push('/cabinet')
        }
        else {
            setErrorState(errors)
        }
    },[inputData])
    if (!props.show) return null
    return (
        <div className="register">
            <h3 className="register_header">Sign Up</h3>
            <form className="register_form" onSubmit={handleSendAction}>
                 <Input type={"text"}  label={"First name"} placeholder={"First name"} changed={(event) => inputOnchangeHandler(event, "firstName")} value={firstName}/>
                <p className="error"> {errorState.firstName}</p>
                <Input type={"text"}  label={"Last name"} placeholder={"Last name"} changed={(event) => inputOnchangeHandler(event, "lastName")} value={lastName}/>
                <p className="error"> {errorState.lastName}</p>
                <Input type={"email"}  label={"Email Address"} placeholder={"Email Address"} changed={(event) => inputOnchangeHandler(event, "email")} value={email}/>
                <p className="error"> {errorState.email}</p>
                 <Input type={"password"}  label={"Password"} placeholder={"Enter your password"} changed={(event) => inputOnchangeHandler(event, "password")} value={password}/>
                <p className="error"> {errorState.password}</p>
                 <CustomBtn class={"SignUpBtn customBtn"} text={"Sign Up"} />
            </form>
        </div>
    );
}

export default Registration;
