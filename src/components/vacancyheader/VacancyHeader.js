import React from 'react';
import image from "../../containers/task4/vacancy.png";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";


const VacancyHeader = (props) => {
    return (
        <div className={"vacancy_header"}>
            <img alt={""} src={image} className={"vacancy_img"}/>
            <div className="vacancy_info">
                <h3 className="">{props.data.name}</h3>
                <p className="company-name"><FontAwesomeIcon icon="building"
                                                             className={"icon"}/> {props.data.companyName || "şirkət qeyd olunmayıb"}
                </p>
            </div>
            <div className={"salary"}>
                <p>{props.data.minSalary && props.data.maxSalary ?
                    props.data.minSalary + " - " + props.data.maxSalary + " azn" :
                    props.data.minSalary ? props.data.minSalary + " azn" :
                        props.data.maxSalary ? props.data.maxSalary + " azn" :
                            "Ə/h razılaşma ilə"
                }</p>
            </div>
        </div>
    );
}

export default VacancyHeader;
