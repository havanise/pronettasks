import React from 'react';
import PropTypes from "prop-types";

const Contact = (props) => {
    const {data} = props
    const {phoneNumber, email, website} = data
    return (
        <div className={"contact"}>
            <div><h3>əlaqə</h3></div>
            <div className={"contact_info"}>
                <div className={"contact_left"}>
                    <p>Telefon:</p>
                    <p>Email:</p>
                    <p>Veb sayt:</p>
                </div>
                <div>
                    <p>{phoneNumber  || "qeyd olunmayıb"}</p>
                    <p>{email  || "qeyd olunmayıb"}</p>
                    <p>{website  ||"qeyd olunmayıb"}</p>
                </div>
            </div>
        </div>
    );
}

Contact.propTypes = {
    data: PropTypes.object
};
export default Contact;
