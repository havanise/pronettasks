import React from 'react';
import "./total.css";
import PropTypes from 'prop-types';

const Total = (props) => {
    return (
        <div className="total">
            <p className="total_price">{props.total}<span className="pink_dollar">$</span></p>
            <p>Total Value of Disbursement</p>
        </div>
    );
}
Total.propTypes = {
    total: PropTypes.number.isRequired,
};
export default Total;
