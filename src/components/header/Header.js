import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import "./header.css"

const Header = () => {
    return (
        <div className="header">
            <ul className="menu">
                <li><a href="#">Overview</a></li>
                <li><a href="#">Policy</a></li>
                <li><a href="#">Reports</a></li>
            </ul>
            <div className="buttons">
                <button className="add_btn">Add new<span className="plus">&#43;</span></button>
                <button className="calendar"><FontAwesomeIcon icon="calendar-alt" className="calendar_icon" color="#7843DE"/>10 Jan - 21 Mar 2020</button>
            </div>
        </div>
    );
}

export default Header;
