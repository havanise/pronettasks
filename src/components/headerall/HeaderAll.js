import React from "react";
import {Link} from "react-router-dom";
import "./headerall.css"

export default function HeaderAll() {
    return(
        <ul className="header_all">
            <Link to="/task1">
                <li>task1</li>
            </Link>
            <Link to="/task2">
                <li>task2</li>
            </Link>
            <Link to="/task3">
                <li>task3</li>
            </Link>
            <Link to="/vacancy">
                <li>task4</li>
            </Link>
            <Link to="/filtered">
                <li>task5</li>
            </Link>
        </ul>
    )
}