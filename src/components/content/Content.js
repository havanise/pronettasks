import React from 'react';
import PropTypes from "prop-types";
import Section from "../section/Section";


const Content = (props) => {
    const {data} = props
    const {city, category, sector, position, education, fromAge, minExperience, maxExperience, genderCONST,
        languages, workGraphic, requirements} = data
    return (
            <div className={"more_info"}>
            <Section icon={"city"} section={"Şəhər:"} value={city ? city.name: "şəhər qeyd olunmayıb"}/>
            <Section section={"Kateqoriya:"} value={category ? category.name: "Kateqoriya qeyd olunmayıb"}/>
            <Section section={"Sahə:"} value={sector ? sector.name: "sahə qeyd olunmayıb"}/>
            <Section section={"Vəzifə:"} value={position ? position.name: "Vəzifə qeyd olunmayıb"}/>
            <Section section={"Təhsil:"} value={education ? education.name: "qeyd olunmayıb"}/>
            <Section section={"Yaş:"} value={fromAge || "Qeyd olunmayıb"}/>
            <Section icon={"briefcase"} section={"İş təcrübəsi:"} value={minExperience && maxExperience ?
                minExperience + " ildən " + maxExperience + " ilə qədər" :
                minExperience ? minExperience + " ildən çox" :
                    maxExperience ? maxExperience + " ilə qədər" :
                        "qeyd olunmayıb"}/>
            <Section section={"Cinsi:"} value={genderCONST  || "qeyd olunmayıb"}/>
            <Section section={"Dil biliyi:"} value={languages  && languages.length ? languages[0].name :"qeyd olunmayıb"}/>
            <Section section={"İş qrafiki:"} value={workGraphic  ? workGraphic.name : "qeyd olunmayıb"}/>
            <Section section={"Namizədə tələblər:"} value={requirements || "qeyd olunmayıb"}/>
            </div>
        )
}
Content.propTypes = {
    data: PropTypes.object
};
export default Content;
