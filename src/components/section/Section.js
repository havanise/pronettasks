import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const Section = ({section, value, icon})=>{
    return(
        <div className={"details"}>
            <p>{icon?<FontAwesomeIcon icon={icon} className={"icon"}/>: null}{section}</p>
            <p>{value}</p>
        </div>
    )
}
export default Section;