import React, {useState} from 'react';
import "./card.css"
import Linear from "../linear/Linear";
import Total from "../total/Total";

const Card = () => {
    const [values, setValues] = useState({
        backgroundPolicy: "#33D69F",
        backgroundClaim:"#713BDB",
        firstLine: "Policy",
        secondLine:"Claim",
        total: 1000,
        policy: 400,
        claim: 800
    });
    let percPolicy= values.policy / values.total * 100;
    let percClaim= values.claim / values.total * 100;
    const [data ,setPercentage] = useState({
        percentage: percPolicy,
        percClaim: percClaim
    })


    return (
        <div className="card">
            <Total total={values.total}/>
            <Linear color={values.backgroundPolicy} price={values.policy} name={values.firstLine} percentage ={data.percentage} />
            <Linear color={values.backgroundClaim} price={values.claim} name={values.secondLine} percentage={data.percClaim}/>
        </div>
    );
}

export default Card;
