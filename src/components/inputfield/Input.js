import React from 'react';
import "./input.css"
import PropTypes from 'prop-types';

const Input = (props) => {
    return (
        <label className="register_form_label">{props.label}
            <input className="register_form_input" onChange={props.changed} type={props.type} placeholder={props.placeholder} value={props.value}/>
        </label>
    );
}

Input.propTypes = {
    label: PropTypes.string,
    type: PropTypes.string,
    placeholder: PropTypes.string
};
export default Input;
