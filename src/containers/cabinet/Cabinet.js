import React from "react";
import { useHistory} from "react-router-dom";
import CustomBtn from "../../components/custombtn/CustomBtn";
import "./cabinet.css"

const Cabinet = () => {
    let history = useHistory();
    const handleLogout = ()=>{
        localStorage.removeItem('user')
        history.push('/task3')
    }
    return(
        <div className="cabinet">
            <CustomBtn text={"Log out"} class={"logout_btn customBtn"} onClick={handleLogout}/>
            <p> hello {localStorage.getItem("user")}</p>
        </div>
    )
}
export default Cabinet;