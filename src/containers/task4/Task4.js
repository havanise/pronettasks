import React, {useEffect} from "react";
import Loader from 'react-loader-spinner'
import "./task4.css"
import image from "./vacancy.png"
import { useHistory} from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {connect} from "react-redux";
import {fetchVacancies} from "../../store/actions/vacanciesActions";

const Task4 = ({vacanciesData,fetchVacancies}) =>{
    let vacancies = vacanciesData.vacanciesData
    useEffect(() => {
        if (!vacancies.length){
        fetchVacancies()}
    }, [])

    let history = useHistory();
    if (vacanciesData.loadingVacancies){
        return <div className={"spinner"}>
            <Loader type="ThreeDots" color="#1b8ef9"/>
        </div>
    }
    else if (vacanciesData.error){
        return <div>
            {vacanciesData.error}
        </div>
    }
    else if (vacanciesData.vacanciesData){
        return(
            <div className="task4">
                <div className={"cards"}>
                    <ul className={"card_list"}>
                        {vacanciesData.vacanciesData.map((object)=>{
                            return (
                                <li key={object.id}>
                                    <div onClick={()=> history.push({ pathname: '/vacancy/' +object.id})} className="card_box" >
                                        <div className={"first_line"}>
                                            <img alt={""} src={image} className={"card_img"}/>
                                            <div className="card_description">
                                                <h3 className="description_header">{object.name}</h3>
                                                <p className="description_p"><span className="company_name"> <FontAwesomeIcon icon="building" className={"icon"}/> {object.companyName? object.companyName : "şirkət qeyd olunmayıb"}</span> <FontAwesomeIcon icon="eye" className={"icon"}/>{object.viewsCount} baxış </p>
                                            </div>
                                        </div>
                                        <p className="date"> <FontAwesomeIcon icon="clock" className={"icon"} />{object.createdAt.substring(0, 10)}</p>
                                    </div>
                                </li>
                            )
                        })
                        }
                    </ul>
                </div>
            </div>
        )}
}
const mapStateToProps = state =>{
    return {
        vacanciesData : state.vacancies
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchVacancies: () =>{dispatch(fetchVacancies())}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Task4);