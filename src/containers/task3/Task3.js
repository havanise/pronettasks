import React, {useState} from "react";
import CustomBtn from "../../components/custombtn/CustomBtn";
import Registration from "../../components/registration/Registration";
import "./task3.css"

export default function Task3() {
    const [showComp, hideComp] = useState( false );
    const formVisible = ()=>{
        hideComp(!showComp)
    }
    return(
        <div className="main">
            <CustomBtn text={"Registration"} class={"registerBtn customBtn"} onClick={formVisible}/>
            <Registration  show ={showComp} hide={hideComp}/>
        </div>
    )
}