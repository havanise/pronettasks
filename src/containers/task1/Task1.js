import React from "react";
import Sidebar from "../../components/sidebar/Sidebar";
import Header from "../../components/header/Header";
import Card from "../../components/card/Card";
import "./task1.css"


export default function Task1() {
    return(
        <div className="task1_main">
            <div className="container">
                <Sidebar/>
                <div className="left_part">
                    <Header/>
                    <div className="rows">
                        <Card/>
                    </div>
                </div>
            </div>
        </div>
    )
}