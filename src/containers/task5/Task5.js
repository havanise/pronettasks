import React, {useEffect, useState} from "react";
import Loader from 'react-loader-spinner';
import {  Layout, Select, Form, Button  } from "antd";
import "antd/dist/antd.css";
import image from "../task4/vacancy.png";
import { useHistory} from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {connect} from "react-redux";
import {
    fetchSelectName,
    fetchSelectSubFunc,
    fetchVacancies,
    fetchFilteredVacanciesFunc
} from "../../store/actions/vacanciesActions";

const Task5 = ({Data,fetchVacancies, fetchSelectName, fetchSelectSubFunc, fetchFilteredVacanciesFunc}) =>{
    const { Option } = Select;
    const {Content, Sider } = Layout;
    const [form] = Form.useForm();
    const [filters, setFilters] = useState({});
    let vacancies = Data.filteredVacancies;
    let formRef = React.createRef();
    useEffect(() => {
        fetchSelectName()
        if (!vacancies.length){
            fetchVacancies()
        }
    }, [])
    let history = useHistory();
    const onCategoryChange = (value) => {
        formRef.current.setFieldsValue({ subcategory: undefined });
        fetchSelectSubFunc(value)
        setFilters({category:value})
    };
    const onSubCategoryChange = (value) => {
        setFilters({...filters, position:value})
    };
    const onFinish = (values) => {
            fetchFilteredVacanciesFunc(filters)
    };
    if (Data.loadingVacancies){
        return <div className={"spinner"}>
            <Loader type="ThreeDots" color="#1b8ef9"/>
        </div>
    }
    else if (Data.error){
        return <div>
            {Data.error}
        </div>
    }
    else if (Data.filteredVacancies){
        return(
                <Layout>
                        <Sider
                            width="25%"
                            theme="light"
                            breakpoint="lg"
                            collapsedWidth="0"
                        >
                                <Form style={{width: "90%", marginLeft: "5%" }} ref={formRef}
                                    form={form} name="control-hooks" layout={"vertical"} onFinish={onFinish}>
                                    <Form.Item
                                        name="category"
                                        label="kateqoriya"
                                    >
                                        <Select
                                            placeholder="kateqoriya"
                                            onChange={onCategoryChange}
                                            allowClear
                                        >
                                            {
                                                Object.keys(Data.category).map(key =>{
                                                    const cat = Data.category[key]
                                                        return (
                                                            <Option key={cat.id} value={cat.id}>{cat.name}</Option>
                                                        )}
                                                )}
                                            }
                                        </Select>
                                    </Form.Item>
                                    <Form.Item
                                        name="subcategory"
                                        label="subkateqoriya"
                                    >
                                        <Select
                                            placeholder={"alt kateqoriya"}
                                            allowClear
                                            onChange={onSubCategoryChange}
                                        >
                                            {
                                                Object.keys(Data.subcategory).map(key =>{
                                                    const cat = Data.subcategory[key]
                                                    return (
                                                        <Option key={cat.id} value={cat.id }>{cat.name}</Option>
                                                    )}
                                                )}
                                            }
                                        </Select>
                                    </Form.Item>
                                    <Form.Item>
                                        <Button type="primary" htmlType="submit">
                                            Filter
                                        </Button>
                                    </Form.Item>
                                </Form>
                        </Sider>

                    <Layout>
                        <Content style={{ margin: '24px 16px 0' }}>
                            <div className="site-layout-background" style={{ padding: 24, minHeight: 560 }}>
                                <div className="task5">
                                <div className={"cards"}>
                                    <ul className={"card_list"}>
                                        {!Data.filteredVacancies.length
                                            ?
                                            <li>Bu kateqoriyada vakansiya yoxdur</li>
                                            :
                                        Data.filteredVacancies.map((object)=>{
                                            return (
                                                <li key={object.id}>
                                                    <div onClick={()=> history.push({ pathname: '/vacancy/' +object.id})} className="card_box" >
                                                        <div className={"first_line"}>
                                                            <img alt={""} src={image} className={"card_img"}/>
                                                            <div className="card_description">
                                                                <h3 className="description_header">{object.name}</h3>
                                                                <p className="description_p"><span className="company_name"> <FontAwesomeIcon icon="building" className={"icon"}/> {object.companyName? object.companyName : "şirkət qeyd olunmayıb"}</span> <FontAwesomeIcon icon="eye" className={"icon"}/>{object.viewsCount} baxış </p>
                                                            </div>
                                                        </div>
                                                        <p className="date"> <FontAwesomeIcon icon="clock" className={"icon"} />{object.createdAt.substring(0, 10)}</p>
                                                    </div>
                                                </li>
                                            )
                                        })
                                        }
                                    </ul>
                                </div>
                            </div>
                            </div>
                        </Content>
                    </Layout>
                </Layout>
        )}
}
const mapStateToProps = state =>{
    return {
        Data : state.vacancies
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchVacancies: () =>{dispatch(fetchVacancies())},
        fetchSelectName: ()=>{dispatch(fetchSelectName())},
        fetchSelectSubFunc: value => dispatch(fetchSelectSubFunc(value)),
        fetchFilteredVacanciesFunc: values => dispatch(fetchFilteredVacanciesFunc(values))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Task5);